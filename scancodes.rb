module Scancodes
  # Specifies keyboard scancodes for virtualbox input
  # Key up is scancode + 0x80
  module_function
  
  SPECIAL = {
    "<bs>" => "0e",
    "<del>" => "53",
    "<enter>" => "1c",
    "<esc>" => "01",
    "<f1>" => "3b",
    "<f2>" => "3c",
    "<f3>" => "3d",
    "<f4>" => "3e",
    "<f5>" => "3f",
    "<f6>" => "40",
    "<f7>" => "41",
    "<f8>" => "42",
    "<f9>" => "43",
    "<f10>" => "44",
    "<f11>" => "57",
    "<f12>" => "58",
    "<return>" => "1c",
    "<tab>" => "0f",
    "<up>" => "48",
    "<down>" => "50",
    "<left>" => "4b",
    "<right>" => "4d",
    "<spacebar>" => "39",
    "<insert>" => "52",
    "<home>" => "47",
    "<end>" => "4f",
    "<pageUp>" => "49",
    "<pageDown>" => "51"
  }
  
  SHIFTED_CHARS = "~!@\#$%^&*()_+{}|:\"<>?QWERTYUIOPASDFGHJKLZXCVBNM"
  
  scancode_index = {
    "1234567890-=" => "02",
    "!@\#$%^&*()_+" => "02",
    "qwertyuiop[]" => "10",
    "QWERTYUIOP{}" => "10",
    "asdfghjkl;'`" => "1e",
    "ASDFGHJKL:\"~" => "1e",
    "\\zxcvbnm,./" => "2b",
    "|ZXCVBNM<>?" => "2b",
    " " => "39"
  }
  
  scancode_map = Hash.new
  scancode_index.each do |key, value|
    i = 0
    key.split("").each do |c|
      scancode_map[c] = (value.to_i(16) + i).to_s(16)
      i+= 1
    end
  end
  SCANCODE_MAP = scancode_map
  
  def get_scancode(hex)
    hex.rjust(2, "0") + " "
  end
  
  def key_up(hex)
    (hex.to_i(16) + 0x80).to_s(16)
  end
  
  
  def gen_scancodes(boot_command)
    result = ""
    boot_command.each do |line|
      if SPECIAL.has_key? line
        result << get_scancode(SPECIAL[line])
        result << get_scancode(key_up(SPECIAL[line]))
      else
        line.each_char do |char|
          shifted = SHIFTED_CHARS.include?(char)
          if shifted
            result << get_scancode("2a")
          end
          result << get_scancode(SCANCODE_MAP[char])
          result << get_scancode(key_up(SCANCODE_MAP[char]))
          if shifted
            result << get_scancode("aa")
          end
        end
      end
    end
    result
  end
end 
