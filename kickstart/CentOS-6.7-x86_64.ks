################################################
#
# Kickstart file to build base CentOS-6.7-x86_64
# LVM with Static Root for EBS
################################################

# Basic Setup

text
skipx
cmdline
install

# Common Configuration
rootpw --iscrypted $6$pjyKbcVrlnEMbdz7$iL5dwdheR7FnkyP4Vawr6G3IjmKUjeEj3PwQNdBEBCU.r/aGhOaaXLDIsrEhcEu3JlhZYvxxXVJP7cch.JoBG0
lang en_GB.UTF-8
keyboard uk
timezone Etc/UTC
network --bootproto=dhcp --device=eth0 --activate --onboot=yes
firewall --disabled
authconfig --enableshadow --passalgo=sha512
firstboot --disable
selinux --permissive
logging --level=info
reboot

# Disk Layout
zerombr
bootloader --location=mbr --timeout=1
clearpart --all --initlabel
ignoredisk --only-use=sda,sdb
part /boot --fstype ext4 --size=400 --asprimary --label BOOT --ondisk=sda
part swap --fstype swap --size=2048 --asprimary --label SWAP --ondisk=sdb
part pv.01 --grow --size=7168 --label PV --ondisk=sdb
volgroup vg_os pv.01
logvol / --vgname=vg_os --percent=30 --grow --size=1 --name=lv_root
logvol /home --vgname=vg_os --percent=10 --grow --size=1 --name=lv_home
logvol /tmp --vgname=vg_os --percent=10 --grow --size=1 --name=lv_tmp
logvol /var --vgname=vg_os --percent=10 --grow --size=1 --name=lv_var
logvol /var/log --vgname=vg_os --percent=36 --grow --size=1 --name=lv_var_log

# Modify Service Startup
services --enabled=ntpd,acpid,sshd
services --disabled=iptables

# Repositories
# The CentOS repos below will always end up with the latest minor version.
repo --cost=1000 --name=CentOS6-Base --mirrorlist=http://mirrorlist.centos.org/?release=6.7&arch=x86_64&repo=os
repo --cost=1000 --name=CentOS6-Updates --mirrorlist=http://mirrorlist.centos.org/?release=6.7&arch=x86_64&repo=updates
repo --cost=1000 --name=EPEL --baseurl=http://dl.fedoraproject.org/pub/epel/6/x86_64/
repo --cost=1 --name=Puppetlabs --baseurl=http://yum.puppetlabs.com/el/6/products/x86_64/
repo --cost=1 --name=Puppet-deps --baseurl=http://yum.puppetlabs.com/el/6/dependencies/x86_64/

# Add all the packages after the base packages
%packages --nobase
audit
bind-libs
bind-utils
bzip2
coreutils
crontabs
dhclient
e2fsprogs
groff
grub
irqbalance
lsof
lvm2
acpid
iputils
man
man-pages
ntp
ntpdate
parted
vim-common
vim-enhanced
vim-minimal
wget
ack
dos2unix
dstat
git
htop
iftop
lynx
nc
rsync
s3cmd
screen
yum-utils
yum-plugin-changelog
yum-plugin-downloadonly
yum-plugin-ps
redhat-lsb
logrotate
rng-tools
tmpwatch
kernel-devel
kernel-headers
cloud-init
puppetlabs-release
facter-2.3.0-1.el6
puppet-3.7.3-1.el6
epel-release
gcc
sudo
openssh
openssh-clients
openssh-server
pciutils
perl
rcs
rpm
setransd
sysstat
tcpdump
telnet
unzip
curl
which
zip
yum
python
hdparm
tcl
tk
dmidecode
openssl-devel
readline-devel
zlib-devel
iotop
ltrace
nano
strace
tree
dos2unix

# Following Packages not Required 
-abrt-*
-avahi
-crda
-fprintd-*
-hunspell-*
-ledmon
-libertas-*-firmware
-libreport-*
-libstoragemgmt
-iwl*-firmware
-ntsysv
-rubygem-abrt
-setuptool
-smartmontools
-usb_modeswitch-*
-yum-langpacks
-biosdevname
-NetworkManager
-checkpolicy
-dhcpv6-client
-ed
-kbd
%end

# The following is used by packer for provisioner actions.
%post

mkdir /root/.ssh
chmod 700 /root/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCRsOlZeVl+R6EqwSPKRWMWNe8nXFAJQfV9ePyxo6HqTHUrf344yoLfe/yIgpea7vvIkc/L/SZuY6N4wazqIorbVLvKY4X+MUOAg9WS/mUr4SgtasZei7zu0DDK/rU/1/VIHYvk8UXCH89qxgyqdqmAgnk+1vfIGKr36aarZ+ognnvl4JgTJlmo27+sRzhU4ukCVno0kGLssv6IOH/K5Vp9eDXYZ12g77V90bY1OBzli9Eq6+cwZypQ9zurKg1bMWq5fwwS/x8Y7sssSGFojJcrqG+8Vh9HUNvSEikSpogYbdpSKfQjpa4g4pwJCMe4WUMVRpf8kb8/PLhiBNi5td6L Vagrant insecure key" > /root/.ssh/authorized_keys
chmod 644 /root/.ssh/authorized_keys
%end

# Disable cloud init.  It'll be reenabled by a provisioner if needed.
%post
chkconfig cloud-config off
chkconfig cloud-final off
chkconfig cloud-init off
chkconfig cloud-init-local off
%end

# Disable RequireTTY for sudo
%post
sed -i 's/\(Defaults.*requiretty\)/# \1/g' /etc/sudoers
sed -i 's/disable_root.*/disable_root: 0/' /etc/cloud/cloud.cfg
%end

# Create stub data mount points
%post
mkdir /data
mkdir /data/00
mkdir /data/01
mkdir /data/02
mkdir /data/03
mkdir /data/04
mkdir /data/05
mkdir /data/06
mkdir /data/07
mkdir /data/08
mkdir /data/09
mkdir /data/10
mkdir /data/11
%end

# Remove IPV6 module
%post
MODPROBE_CONF="/etc/modprobe.d/blacklist-ipv6.conf"
rm -f "/etc/modprobe.d/disable_ipv6.conf"

cat >> $MODPROBE_CONF << EOF
alias net-pf-10 off
alias ipv6 off
install ipv6 disable 1
EOF

sed -i 's/\(^NETWORKING_IPV6=\)yes$/\1no/g' /etc/sysconfig/network

for eth in$(ls /etc/sysconfig/network-scripts/ifcfg-eth*); do
sed -i 's/IPV6INIT=yes/IPV6INIT=no/'
EOF
done

chkconfig ip6tables off
%end

# Tune IO Q
%post
echo 512 > /sys/block/sda/queue/nr_requests
echo 254 > /sys/block/sda/device/queue_depth
/sbin/blockdev --setra 1024 /dev/sda
%end

# Update limits.conf for Hadoop
%post
cat >> /etc/security/limits.d/hadoop << EOF
* - nofile 32768
* - nproc 65536
EOF

# Sysctl for Hadoop nodes
%post
cat >> /etc/sysctl.conf << EOF
net.core.netdev_max_backlog = 4000
net.core.somaxconn = 4000

net.ipv4.ip_forward = 0
net.ipv4.default.rp_filter = 1
net.ipv4.default.accept_source_route = 0
net.ipv4.tcp_syscookies = 1
net.ipv4.tcp_sack = 1
net.ipv4.tcp_keepalive_time = 600
net.ipv4.tcp_keepalive_probes = 5
net.ipv4.tcp_keepalive_intvl = 15
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_rmem = 32768 436600 41934404
net.ipv4.tcp_wmem = 32768 436000 41934404
net.ipv4.tcp_retries2 = 10
net.ipv4.tcp_synack_retries = 3

net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1

kernel.sysrq = 0
kernel.core_uses_pid = 1
kernel.msgmnb = 65536
kernel.msgmax = 65536
kernel.shmmax = 68719476736
kernel.shmall = 4294967296
vm.swappiness = 1
EOF
%end

# Modification for mke2fs.conf to set correct params for hadoop data disks
%post
cat << 'EOF' >> /etc/mke2fs.conf
	hadoop = {
		features = has_journal,extent,huge_file,flex_bg,uninit_bg,dir_nlink,extra_isize
		inode_ration = 131072
		blocksize = -1
		reserved_ration = 0
		default_mntopts = acl,user_xaddr
	}
EOF

%end

