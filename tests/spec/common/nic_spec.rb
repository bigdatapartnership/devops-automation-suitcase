# Ensure NIC config.

require 'spec_helper'

describe file('/etc/sysconfig/network-scripts/ifcfg-eth0') do
  its(:content) { should_not match /^UUID.*/ }
  its(:content) { should_not match /^HWADDR.*/ }
end
