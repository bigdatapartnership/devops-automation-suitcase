# Ensure NIC config.

require 'spec_helper'

describe file('/etc/init.d/resize-partition') do
  its(:content) { should match /^start_point.*/ }
end
