export PATH=/cygdrive/c/Program\ Files/Oracle/VirtualBox:$PATH

# Upload ks to s3://bdpsuitcase/kickstart/CentOS-6.7-x86_64.ks
# set permissions for everyone to download and make bucket a website

Create VirtualBox with a 1/2GB root disk and a second 10GB disk, boot via minimal CentOS 6.7 DVD, but on boot screen press TAB -> then clear the line and press enter.
On the Grub screen type:
linux ks=http://bdpsuitcase.s3-website.eu-central-1.amazonaws.com/kickstart/CentOS-6.7-x86_64.ks

Then leave to install; when it reboots (if doing manual method not suitcase) terminate reboot, remove DVD then boot forwarding port 2222 for ssh

run the extra scripts from suitcase then the tests

Turn off VM

VBoxManage clonehd $IMAGE_FILE_VB $IMAGE_FILE --format RAW

VBoxManage clonehd "/Users/Mark/VirtualBox VMs/CentOS-6.7/CentOS-6.7.vdi" CentOS-6.7-x86_64.img --format RAW

VBoxManage clonehd "/Users/Mark/VirtualBox VMs/CentOS-6.7/CentOS-6.7-root.vdi" CentOS-6.7-root-x86_64.img --format RAW


gzip disks

transfer to aws
boot a amazon linux image and attach two disks unformatted one 1G and one 10G to /dev/xdv[f|g]
gunzip images and write to disks
sudo dd if=CentOS-6.7-x86_64.img of-/dev/xvdf bs=10M
sudo dd if=CentOS-6.7-ebs-x86_64.img of=/dev/xvdg bs=10M

fdisk /dev/xvdb
d
c
n
p
2


w

DISK_SIZE
pvresize /dev/xvdb2
lvextend -l +30%FREE /dev/vg_os/lv_root
lvextend -l +30%FREE /dev/vg_os/lv_home
lvextend -l +30%FREE /dev/vg_os/lv_var
lvextend -l +30%FREE /dev/vg_os/lv_tmp
lvextend -l +80%FREE /dev/vg_os/lv_var_log
resize2fs /dev/mapper/vg_os-lv_root
resize2fs /dev/mapper/vg_os-lv_home
resize2fs /dev/mapper/vg_os-lv_var
resize2fs /dev/mapper/vg_os-lv_tmp
resize2fs /dev/mapper/vg_os-lv_var_log


## Image Creation script progress ##

create_vm.rb in src/ currently does the following:

* Creates a new VM and attaches two disks of the specified size
* Inputs the details for a kickstart file
* Detects when the installation is complete and removes the attached iso
* Uploads the scripts and tests, runs them
* Stops the VM, converts the disk formats and gzips them

TODO:
* create instance from the ami built
* run serverspec tests on that
* make sure ec2-user and puppet stuff is set up correctly

scancodes.rb handles the input of the kickstart url to virtualbox.
