Suitcase
=============

## Source Project
This project is a fork from the good work done by https://github.com/tmclaugh/suitcase, we are looking to extend the project to remove the dependancy on Packer which limits the ability to use LVM based images and also to be able to build HSM based AMI's. In the future we are also looking to be able to be able to automatically build Images for Microsoft Azure and Google Compute, Along with images suitable for Internal use and Openstack.
Please feel free to submit patches or open issues. We intend to to provide assistance and development as time permits to maintain this project going forward.

You can email us at suitcase@bigdatapartnership.com.


---------
Building images is a multi-step process using VirtualBox and AWS. All work is performed on a developers local system.

## Setup
The process of building images requires:
* [VirtualBox](https://www.virtualbox.org/)
* AWS cli
* AWS IAM credentials to access S3.
* Ruby


## Process
The tool reads the config file config/defaults.yml by default. All of these settings can be overwritten by providing a config file as an argument (with -f or --config-file), following the same format.

Using the provided config, it spins up a VM using VirtualBox, supplying the kickstart file. After the OS install the scripts and tests are run in order. When they pass, the volumes are zipped and uploaded to an EC2 instance - from here they are written to disk.

Snapshots are taken and used to create an AMI, which is the finished product. You should be able to log into it as the root user with the key generated.

### Example usage

ruby create_vm.rb -f config/CentOS-6.7.yml -k -p

The above will provision a CentOS 6.7 image and create an AMI out of it. The VM will be kept and a key with the given name will be kept and not overwritten.

## Repo Layout
* files/
    * Ancilary files for building images or using this repo.
* images/
    * Images for different platforms.
* kickstart/
    * Kickstart files for building master image.
    * These files should be versioned by X.Y (ex. 6.5, 6.6) so older images can be reproduced.
* scripts/
    * Scripts to be run by templates during provisioners phase.  Scripts are not automatically run and must be specified in the build template.
    * The subdirectories under _scripts/_ correspond to the image type being built.  Scripts in _images/common/_ are meant to be executed by all or most image types.
* tests/
    * spec testing framework for image verification.
    * See below for more information on the testing framework.
* disks/
    * This is the default folder that VirtualBox will use for the virtual disks in use by the VM.
* scancodes.rb
    * This ruby module converts an input string to a sequence of keyboard scancodes for use by virtualbox.
* create\_vm.rb
    * All application logic is handled here.

Examples:
* os template: CentOS-6.5-x86_64.json
* image template: vagrant.json

## Config
The configuration uses yaml, and an example of it is below. Everything inherits from the default config file, and any fields included in both will be overwritten.


```yaml
base:
  VBOX_HOME: "/home/ben/VirtualBox\ VMs"
  VDI_LOCATION: "disks"
  ISO_LOCATION: "iso/default.iso"
  ZIP_DISK_LOCATION: "files/"
  PROVIDER: aws

vm:
  VM_NAME: suitcase
  VM_MEMORY: 1024
  VM_USER: root
  HOST_PORT: 22222
  GUEST_PORT: 22
  SCRIPT_LOCATION: "/tmp"
  OS_TYPE: RedHat_64

hdd:
  PRIMARY_HDD_SIZE: 512
  SECONDARY_HDD_SIZE: 10240

aws:
  KEY_NAME: suitcase-key
  IMAGE_ID: ami-daaeaec7
  MAPPING_FILE: "files/mapping.json"
  SECURITY_GROUP: sg-6ddd5204
  REGION: eu-central-1
  SUBNET_ID: subnet-4e202136
  AMI_NAME: suitcase-test

kickstart:
  KICKSTART_FILE: CentOS-6.7-x86_64.ks
  BUCKET_NAME: bdpsuitcase 
  KICKSTART_COMMAND:
    - <tab>
    - " linux ks=http://bdpsuitcase.s3-website.eu-central-1.amazonaws.com/kickstart/CentOS-6.7-x86_64.ks"
    - <enter>

scripts:
  - scripts/common/nic.sh
  - scripts/aws/sriov.sh

tests:
  - tests/serverspec.sh

post:
  - scripts/common/cleanup.sh

```

## Testing
This repo provides a [Serverspec](http://serverspec.org/) based testing framework to ensure that images conform to a desired specification. Tests can be specified under the "tests" config heading - by default tests/serverspec.sh will be run. 

All tests are in subdirectories under the _tests/spec/_ directory. 

## Usage
To use the program, you must first download a CentOS minimal disk image that corresponds with the existing kickstart files. Set the location of it in the config.

Then simply run:
```
ruby create_vm.rb
```
This will create a VM based on the default config. To specify a particular config, use the "-f" or "--config-file" options. 

More options are available if the program is run with the "-h" option.

