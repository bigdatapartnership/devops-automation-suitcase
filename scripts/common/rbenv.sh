#!/bin/bash
set -x

# Download the latest version of rbenv
git clone https://github.com/sstephenson/rbenv.git /opt/rbenv

# Install ruby-build as a rbenv plugin
mkdir -p /opt/rbenv/plugins/
git clone https://github.com/sstephenson/ruby-build.git /opt/rbenv/plugins/ruby-build

# Create the rbenv profile
RBENV_PROFILE=/etc/profile.d/rbenv.sh

cat << "EOF" > $RBENV_PROFILE
export PATH=/opt/rbenv/bin:$PATH
export RBENV_ROOT=/opt/rbenv
EOF

cat $RBENV_PROFILE >> ~/.bash_profile

source $RBENV_PROFILE
echo 'eval "$(rbenv init -)"' >> $RBENV_PROFILE
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
source ~/.bash_profile

cd /tmp/tests/

rbenv rehash

rbenv install 2.2.3

rbenv rehash

rbenv local 2.2.3

rbenv rehash
