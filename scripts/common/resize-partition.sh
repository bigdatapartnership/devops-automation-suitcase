#!/bin/bash -x

INIT_SCRIPT="/etc/init.d/resize-partition"

cat << "EOF" > $INIT_SCRIPT
#!/bin/bash
# chkconfig: 234 24 24
# description: Resizes a partion and the lvm volumes on top to match the disk size
start_point="$(parted -m /dev/xvdb print | grep ^2 | cut -d':' -f2)"
end_point="$(parted -m /dev/xvdb print | grep ^2 | cut -d':' -f3)"
max="$(parted -m /dev/xvdb print | grep "/dev/xvdb" | cut -d':' -f2)"

echo "start_point=$start_point"
echo "end_point=$end_point"
echo "max=$max"

if [[ $end_point != $max ]]; then
  echo "not equal, resizing"
  parted /dev/xvdb rm 2
  parted /dev/xvdb mkpart primary $start_point $max
  reboot
else
  pvsize="$(pvs --unit G | awk '/xvdb/{print $5}' | cut -d'.' -f1)"
  disksize="$(parted -m /dev/xvdb print | grep ^2 | cut -d':' -f4 | cut -d'.' -f1)"

  if [[ $pvsize != $disksize ]]; then
    pvresize /dev/xvdb2
  fi

  lv_root_size="$(lvs -o lv_name,lv_size --units b | awk '/lv_root /{print $2}' | sed 's/.$//')"
  lv_var_size="$(lvs -o lv_name,lv_size --units b | awk '/lv_var /{print $2}' | sed 's/.$//')" 
  five=5368709120

  if [[ $lv_root_size -lt $five ]]; then
    amount=$(expr $five - $lv_root_size)
    lvextend -L +"$amount"B /dev/vg_os/lv_root
    resize2fs /dev/vg_os/lv_root
  fi
  if [[ $lv_var_size -lt $five ]]; then
    amount=$(expr $five - $lv_var_size)
    lvextend -L +"$amount"B /dev/vg_os/lv_var
    resize2fs /dev/vg_os/lv_var
  fi
  
  chkconfig resize-partition off
fi
EOF

chmod 755 $INIT_SCRIPT

chkconfig --add $INIT_SCRIPT
chkconfig atd off
