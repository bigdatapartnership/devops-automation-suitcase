#!/usr/bin/ruby
#
# Load the config file here
#

require 'json'
require 'yaml'
require 'optparse'

require_relative 'scancodes'



def read_config(path)
  config_file = File.open(path)
  config = YAML.load(File.read(config_file))
  config
end

def set_value(name, dict, key)
  if dict.has_key?(key)
    instance_variable_set("@#{name}", dict[key])
  end
end

def set_config(config)
  # Sets config options based on the read config file.
  # Falls back to the default values if there is nothing in the yaml.
  %w(vbox_home vdi_location iso_location zip_disk_location provider).each do |b|
    set_value(b, config["base"], b.upcase)
  end

  %w(vm_name vm_memory vm_user os_type host_port guest_port script_location).each do |v|
    set_value(v, config["vm"], v.upcase)
  end

  %w(primary_hdd_size secondary_hdd_size).each do |h|
    set_value(h, config["hdd"], h.upcase)
  end

  %w(key_name image_id mapping_file security_group region subnet_id ami_name).each do |a|
    set_value(a, config["aws"], a.upcase)
  end

  %w(kickstart_file bucket_name kickstart_command).each do |k|
    set_value(k, config["kickstart"], k.upcase)
  end

  instance_variable_set("@#{'scripts'}", config["scripts"])
  instance_variable_set("@#{'tests'}", config["tests"])
  instance_variable_set("@#{'post'}", config["post"])


end

class VBox
  %w(createvm list modifyvm controlvm storagectl storageattach createmedium
      unregistervm clonehd startvm closemedium).each do |action|
    define_method("#{action}") do |args|
      `vboxmanage #{action} #{args}`
    end
  end
end

class Aws
  def initialize(region)
    @region = region
  end

  %w(ec2 s3api).each do |action|
    define_method("#{action}") do |args|
     `aws #{action} --region #{@region} #{args}`
    end
  end
end

def create_vm
  puts "Creating VM..."
  V.createvm(" --name #{@vm_name} --register --ostype #{@os_type}")
  
  puts "Changing VM settings"
  V.modifyvm("#{@vm_name} --memory #{@vm_memory} \
                          --natpf1 'guestssh',tcp,,#{@host_port},,#{@guest_port} \
                          --vram 51")
  
  puts "Adding storage controllers"
  V.storagectl("#{@vm_name} --name IDE --add ide --bootable on")
  V.storagectl("#{@vm_name} --name SATA --add sata --bootable on")
  
  puts "Creating the vdis"
  V.createmedium("--filename #{@primary_hdd} \
                  --size #{@primary_hdd_size} ")
  V.createmedium("--filename #{@secondary_hdd} \
                  --size #{@secondary_hdd_size} ")
  
  puts "Attaching the storage"
  V.storageattach("#{@vm_name} --storagectl IDE --type dvddrive --port 0 --device 0 \
                              --medium #{@iso_location}")
  V.storageattach("#{@vm_name} --storagectl SATA --type hdd --port 0 --device 0 \
                              --medium '#{@primary_hdd}.vdi'")
  V.storageattach("#{@vm_name} --storagectl SATA --type hdd --port 1 --device 0 \
                              --medium '#{@secondary_hdd}.vdi'")
end


def start_vm
  puts "Starting VM"
  V.startvm("#{@vm_name} --type gui")
  sleep(20)
end

def stop_vm
  puts "Shutting down VM #{@vm_name}"
  V.controlvm("#{@vm_name} poweroff --soft")
end

def delete_vm
  stop_vm

  puts "Deleting the VM"
  V.unregistervm("#{@vm_name} --delete")

  # Split the output into blocks
  hddinfo = V.list("hdds").split(/$\n\n/)
  media = []
  hddinfo.each do |block|
    b = Hash.new
    block.split("\n").each do |line|
      # Create a hash for each block for easy lookup

      l = line.split(":")

      # Input the leading part of the line as the key to the hash,
      # and the trailing part as the value
      w = []
      l.each do |part|
        w << part.strip
      end
      b[w[0]] = w[1]
    end
    media << b
  end

  # Remove all disks from the registry with the vm name we're using
  media.each do |m|
    if /#{@vm_name}_.*\.img/.match(m["Location"])
      puts "Deleting disk with UUID #{m["UUID"]}"
      V.closemedium("#{m["UUID"]} --delete")
    end
  end
end


def input_ks
  puts "Inputting the kickstart command"
  puts @kickstart_command
  Scancodes.gen_scancodes(@kickstart_command).split.each do |scancode| 
    V.controlvm("#{@vm_name} keyboardputscancode #{scancode}")
  end
  puts "Installing the system, this will take a while"
end

def remove_install_media
  installed = false
  until installed do
    # The following indicates that the install has finished and the restart was completed.
    `grep "Changing the VM state from 'RESETTING' to 'RUNNING'" "#{@vbox_home}/#{@vm_name}/Logs/VBox.log"` 
    if $? == 0
      installed = true
      puts "Machine has been reset."
      puts "The VM will be restarted to remove the install media."
      break
    end
  end
  
  stop_vm
  
  puts "Removing install media"
  V.storageattach("#{@vm_name} --storagectl 'IDE' --port 0 --device 0 --medium none")

  start_vm
end

def upload_scripts
  puts "Uploading the scripts and tests."

  `scp -o UserKnownHostsFile=/dev/null \
       -o StrictHostKeyChecking=no \
       -i files/hsimage_root_rsa \
       -P #{@host_port} \
       -r scripts #{@vm_user}@127.0.0.1:#{@script_location}`
  
  `scp -o UserKnownHostsFile=/dev/null \
       -o StrictHostKeyChecking=no \
       -i files/hsimage_root_rsa \
       -P #{@host_port} \
       -r tests #{@vm_user}@127.0.0.1:#{@script_location}`
end

def send_command(key, port, host, command)
  o = `ssh -t -o UserKnownHostsFile=/dev/null \
      -o StrictHostKeyChecking=no \
      -i #{key} \
      -p #{port} \
      #{host} \
      '#{command}'`
  puts o
  $?
end

def send_vm_command(command)
  send_command("files/hsimage_root_rsa", @host_port, "#{@vm_user}@127.0.0.1", command)
end

def send_aws_command(ip, command)
  send_command("files/#{@key_name}.pem", 22, "ec2-user@#{ip}", command)
end

def run_scripts
  puts "Running the scripts - this might take a while"
  command = "cd /tmp/;
             source /common/os_version.sh;" 
  @scripts.each do |script|
    command << "./#{script};"
  end

  send_vm_command(command)

end

def run_tests
  puts "Running the tests"

  command = "cd /tmp/;"
  @tests.each do |t|
    command << "./#{t};"
  end

  send_vm_command(command)

  if $? != 0
    puts "Tests failed, aborting."
    return 1
  end

  return 0
end

def post_scripts
  puts "Running cleanup scripts."

  command = "cd /tmp/;"
  @post.each do |p|
    command << "./#{p};"
  end

  send_vm_command(command)

end

def gzip_disks
  stop_vm

  img = "#{@zip_disk_location}/#{@vm_name}"

  puts "Cloning disks and converting to RAW format"
  V.clonehd("#{@primary_hdd}.vdi #{img}_1.img --format RAW")
  V.clonehd("#{@secondary_hdd}.vdi #{img}_2.img --format RAW")

  puts "Gzipping disks"
  `gzip -f #{img}_1.img`
  `gzip -f #{img}_2.img`
end

def upload_kickstart_file
  puts "Uploading the kickstart file to s3"
  AWS.s3api("put-object --acl public-read --bucket #{@bucket_name} \
  --key kickstart/#{@kickstart_file} --body kickstart/#{@kickstart_file}") 
end

def create_key_pair
  puts "Creating key pair #{@key_name}"
  AWS.ec2("create-key-pair --key-name #{@key_name} \
   --query 'KeyMaterial' --output text > files/#{@key_name}.pem")
  File.chmod(0700, "files/#{@key_name}.pem")
end

def create_instance
  # Key pair must already exist
  puts "Creating AWS instance with EBS volumes matching the primary/secondary hdd sizes"
  id = AWS.ec2("run-instances --image-id #{@image_id} --count 1 \
        --instance-type 't2.micro' --security-group-ids #{@security_group}  \
        --key-name #{@key_name} --block-device-mappings 'file://#{@mapping_file}' \
        --query 'Instances[0].InstanceId' --subnet-id #{@subnet_id} \
       ")
  id = format(id)
  puts "Created instance with id #{id}"

  puts "Getting IP address"
  sleep(10)
  ip = AWS.ec2("describe-instances --instance-ids '#{id}' --query 'Reservations[0].Instances[0].PublicIpAddress'")
  ip = format(ip)
  puts "IP is #{ip}"

  return ip, id
end

def set_tags
  puts "Setting the name of the instance"
end

def upload_disks(ip)
  puts "Uploading the disk images to the instance"
  `scp -o UserKnownHostsFile=/dev/null \
       -o StrictHostKeyChecking=no \
       -i files/#{@key_name}.pem \
        files/*.gz ec2-user@#{ip}:/home/ec2-user/`
end

def write_disks(ip)
  puts "Writing disk images to EBS volumes"
  command = "gunzip -c #{@vm_name}_1.img.gz | sudo dd of=/dev/xvdb bs=10M;\
             gunzip -c #{@vm_name}_2.img.gz | sudo dd of=/dev/xvdc bs=10M;\
             rm *.gz;"

  send_aws_command(ip, command)
  puts "Wrote disk images."
end

def format(str)
  str.tr('"','').strip
end

def get_volume_ids(id)
  puts "Getting volume ids"
  vols = Hash.new
  out = AWS.ec2("describe-instances --instance-ids #{id} --query 'Reservations[0].Instances[0].BlockDeviceMappings'")
  j = JSON.parse(out)
  j.each do |ebs|
    vol_id = format(ebs["Ebs"]["VolumeId"])
    mount = format(ebs["DeviceName"])
    vols[mount] = vol_id
  end
  vols.delete("/dev/xvda") # We don't need the root volume of the instance
  puts vols
  vols
end

def detach_volumes(id, vols)
  vols.each do |mount, vol|
    puts "Detaching #{vol} attached at #{mount}"
    AWS.ec2("detach-volume --instance-id #{id} --volume-id #{vol}")
  end
  puts "All volumes detached"
end

def delete_key_pair
  puts "Deleting existing key pair"
  AWS.ec2("delete-key-pair --key-name #{@key_name}")
end

def create_ebs_snapshots(vols)
  puts "Creating snapshots of the EBS volumes"
  snapshots = Hash.new
  vols.each do |mount, vol|
    snapshots[mount] = format(AWS.ec2("create-snapshot --volume-id #{vol} --query 'SnapshotId'"))
  end
  puts snapshots
  snapshots
end

def generate_mapping(snapshots)
  blockdevices = []
  snapshots.each do |mount, snapshot|
    # Let's hope that nobody tries this with any disks past y
    m = mount.sub("s", "xv")
    m[-1] = (m[-1].ord - 1).chr 
    vol = {
      :DeviceName => m,
      :Ebs => {
        :SnapshotId => snapshot,
        :VolumeType => "gp2",
        :DeleteOnTermination => true,
      }
    }
    if m == "/dev/xvda"
      vol[:VirtualName] = "root"
    end
    
    blockdevices << vol
  end

  mapping = {
    :Name => "#{@vm_name}",
    :Description => "#{@os_type}",
    :VirtualizationType => "hvm",
    :RootDeviceName => "/dev/xvda",
    :BlockDeviceMappings => blockdevices,
    :Architecture => "x86_64",
  }
  puts JSON.pretty_generate(mapping)

  mapping.to_json
end

def register_ami(snapshots)
  puts "Registering an AMI from the snapshots"
  input =  generate_mapping(snapshots)
  ami = format(AWS.ec2("register-image --cli-input-json '#{input}' --query 'ImageId'"))
  puts "AMI has been created with id #{ami}"
  ami
end

def check_snapshot_state(snapshots)
  puts "Waiting for the right snapshot state."
  ready = false
  while ready != true
    count = 0
    snapshots.each do |mount, snapshot|
      puts "Getting snapshot state for #{snapshot}"
      state = format(AWS.ec2("describe-snapshots --snapshot-ids #{snapshot} --query 'Snapshots[0].State'"))
      if state == "completed"
        count += 1
        puts "#{snapshot} in state completed"
      else
        puts "#{snapshot} in state #{state}"
      end
    end
  
    if count == snapshots.length
      ready = true
    end
  
    sleep(15)
  end
end

def create_tag(resource, name)
  AWS.ec2("create-tags --resources '#{resource}' --tags 'Key=Name,Value=#{name}'")
end

def check_ssh(target, ip)
  ssh_access = false

  until ssh_access do
    if target == "aws"
      send_aws_command(ip, "echo 'SSH access confirmed'")
    else
      send_vm_command("echo 'SSH access confirmed'")
    end

    if $? == 0
      ssh_access = true
    end

    sleep(10)
  end
end

def check_instance_status
  puts "Waiting for the instance to start up"

  state = ""
  while state != "ok"
    puts "Checking instance status"
    state = format(AWS.ec2("describe-instance-status --instance-ids #{id} --query 'InstanceStatuses[0].SystemStatus.Status'"))
    puts "Instance is in state #{state}"
    sleep(30)
  end
end

def tear_down(id, vols)
  puts "Removing instance and volumes"
  vols.each do |mount, vol|
    AWS.ec2("delete-volume --volume-id '#{vol}'")
  end
  AWS.ec2("terminate-instances --instance-ids #{id}")
end


options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: create_vm.rb [options]"
  
  opts.on("-f", "--config-file PATH", "Config file") { |v| options["config_file"] = v }
  opts.on("-k", "--keep-vm", "Don't delete the vm") { |v| options["keep_vm"] = v}
  opts.on("-p", "--preserve-key-pair", "Preserve an existing key pair") { |v| options["preserve_key"] = v }

end.parse!



default_path = "config/defaults.yml"
default = read_config(default_path)
set_config(default)

if options.has_key?("config_file")
  config = read_config(options["config_file"])
  set_config(config)
end

@primary_hdd = "#{@vdi_location}/#{@vm_name}_1"
@secondary_hdd = "#{@vdi_location}/#{@vm_name}_2"

V = VBox.new
if @provider == "aws"
  AWS = Aws.new(@region)
end

create_vm
start_vm
upload_kickstart_file
input_ks
remove_install_media
check_ssh("vm", "127.0.0.1")
upload_scripts
run_scripts
res = run_tests
if res == 1
  abort
end
post_scripts

gzip_disks

if options.has_key?("keep_vm")
  puts "Keeping VM #{@vm_name}"
else
  delete_vm
end

if options.has_key?("preserve_key")
  puts "Preserving key pair #{@key_name}"
else
  delete_key_pair
  create_key_pair
end

ip,id = create_instance
create_tag(id, "suitcase")

vols = get_volume_ids(id)
check_ssh(@provider, ip)
upload_disks(ip)
write_disks(ip)
#detach_volumes(id, vols)

snapshots = create_ebs_snapshots(vols)
snapshots.each do |m, s|
  create_tag(s, @ami_name)
end
check_snapshot_state(snapshots)


ami = register_ami(snapshots)
create_tag(ami, @ami_name)

#tear_down(id, vols)
